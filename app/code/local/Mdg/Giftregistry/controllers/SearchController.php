<?php

class Mdg_Giftregistry_SearchController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();

        return $this;
    }

    public function resultsAction()
    {
        $this->loadLayout();
        if ($searchParams = $this->getRequest()->getParam('search_params')):
            $results = Mage::getModel('mdg_giftregistry/entity')->getCollection();

            if ($searchParams['type']):
                $results->addFieldToFilter('type_id', $searchParams['type']);
            endif;

            if ($searchParams['date']):
                $results->addFieldToFilter('event_date', $searchParams['date']);
            endif;

            if ($searchParams['location']):
                $results->addFieldToFilter('event_location', $searchParams['location']);
            endif;

            $this->getLayout()->getBlock('mdg_giftregistry.search.results')
                 ->setCustomerRegistries($results);
        endif;
        $this->renderLayout();

        return $this;
    }
}