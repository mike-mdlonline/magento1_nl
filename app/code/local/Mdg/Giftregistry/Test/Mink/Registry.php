<?php

class Mdg_Giftregistry_Test_Mink_Registry extends JR_Mink_Test_Mink
{
    public function testAddProductToRegistry()
    {
        $this->section('TEST ADD PRODUCT TO REGISTRY');
        $this->setCurrentStore('default');
        $this->setDriver('goutte');
        $this->context();

        // Go to home page
        $this->output($this->bold('Go to the homepage'));
        $url = Mage::getStoreConfig('web/unsecure/base_url');
        $this->visit($url);

        $category = $this->find('css', '#nav .nav-1-1 a');
        if (!$category) {
            return false;
        }

        // Go to login page
        $loginUrl = $this->find('css', 'ul.links li.last a');
        if ($loginUrl) {
            $this->visit(($loginUrl->getAttribute('href')));
        }

        // Getting attributes by CSS selector
        $login  = $this->find('css', '#email');
        $pwd    = $this->find('css', '#pass');
        $submit = $this->find('css', '#send2');

        // Validating whether they are set, if so trying to submit the field
        if ($login && $pwd && $submit) {
            $email      = 'mike.vandiepen@hotmail.com';
            $password   = '';
            $this->output(sprintf('Try to authenticate "%s" with password "%s"', $email, $password));

            $login->setValue($email);
            $pwd->setValue($password);
            $submit->click();

            // Reporting whether the action was successfully
            $this->attempt(
                $this->find('css', 'div.welcome-msg'),
                'Customer successfully logged in',
                'Error authenticating customer'
            );
        }

        // Go to the category page
        $this->output($this->bold('Go to the category list'));
        $this->visit($category->getAttribute('href'));

        $product = $this->find('css', '.category-products li.first a');
        if (!$product) {
            return false;
        }

        // Go to product view
        $this->output($this->bold('Go to product view'));
        $this->visit($product->getAttribute('href'));

        $form = $this->find('css', '#product_registry_form');
        if ($form) {
            $addToCartUrl = $form->getAttribute('action');
            $this->visit($addToCartUrl);
            $this->attempt(
                $this->find('css', '#btn-add-giftregistry'),
                'Product added to gift registry successfully',
                'Error adding product to gift registry'
            );
        }
    }
}