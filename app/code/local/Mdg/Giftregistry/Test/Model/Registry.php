<?php

class Mdg_Giftregistry_Test_Model_Registry extends EcomDev_PHPUnit_Test_case
{
    /**
     * Listing available registries
     *
     * @test
     * @loadFixture
     * @doNotIndexAll
     * @dataProvider dataProvider
     */
    public function registryList()
    {
        $customer_id    = 1;
        $registryList   = Mage::getModel('mdg_giftregistry/entity')
                              ->getCollection()
                              ->AddFieldToFilter('customer_id', $customer_id);
        $this->assertEquals(
            $this->_getExpectations()->getCount(),
            $this->_getExpectations()->getCount(),
            $registryList->count()
        );
    }

    /**
     * Listing available items for a specific registry
     *
     * @test
     * @loadFixture
     * @doNotIndexAll
     * @dataProvider dataProvider
     */
    public function registryItemsList()
    {
        $customerId = 1;
        $registry   = Mage::getModel('mdg_giftregistry/entity')->loadByCustomerId($customerId);

        $registryItems = $registry->getItems();
        $this->assertEquals(
            $this->_getExpectations()->getCount(),
            $registryItems->count()
        );
    }
}