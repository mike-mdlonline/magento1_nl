<?php

class Mdg_Giftregistry_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getEventTypes()
    {
        return Mage::getModel('mdg_giftregistry/type')->getCollection();
    }

    public function isRegistryOwner($registryCustomerId)
    {
        $currentCustomer = Mage::getSingleton('customer/session')->getCustomer();

        return ($currentCustomer && $currentCustomer->getId() == $registryCustomerId) ? true : false;
    }
}
