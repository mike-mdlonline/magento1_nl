<?php

class Mdg_Giftregistry_Model_Api extends Mage_Api_Model_Resource_Abstract
{
    public function getRegistryList($customerId = null)
    {
        $registryCollection = Mage::getModel('mdg_giftregistry/entity')->getCollection();
        if ($customerId != null):
            $registryCollection->addFieldToFilter('customer_id', $customerId);
        endif;

        return $registryCollection;
    }

    public function getRegistryInfo($registryId)
    {
        if ($registryId != null):
            if ($registry = Mage::getModel('mdg_giftregistry/entity')->load($registryId)):
                return $registry;
            endif;

            return false;
        endif;

        return false;
    }

    public function getRegistryItems($registryId)
    {
        if ($registryId != null):
            $registryItems = Mage::getModel('mdg_giftregistry/entity')->getCollection();
            $registryItems->addFilterToField('registry_id', $registryId);

            return $registryItems;
        else:
            return false;
        endif;
    }

    public function getRegistryItemInfo($registryItemId)
    {
        if ($registryItemId != null):
            if ($registryItem = Mage::getModel('mdg_giftregistry/entity')->load($registryItemId)):
                return $registryItem;
            else:
                return false;
            endif;
        else:
            return false;
        endif;
    }
}