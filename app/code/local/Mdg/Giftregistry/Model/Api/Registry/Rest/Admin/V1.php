<?php

class Mdg_Giftregistry_Model_Api_Registry_Rest_Admin_V1 extends Mage_Catalog_Model_Api2_Product_Rest
{
    protected function _retrieve()
    {
        // Registry collection
        return Mage::getModel('mdg_giftregistry/entity')->getCollection();
    }
}